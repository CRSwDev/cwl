requirements:
  InlineJavascriptRequirement: {}
hints:
- class: 'DockerRequirement'
  dockerPull: bdgenomics/rhapsody:2.3
cwlVersion: v1.2
class: CommandLineTool
baseCommand:
- PhiXContamination.sh
inputs:
  Fastq:
    type: File
    inputBinding:
      position: 1
  Threads:
    type: int?
    inputBinding:
      prefix: --threads
outputs:
  PhiX_Contamination:
    type: File
    outputBinding:
      glob: phix_contamination.txt
