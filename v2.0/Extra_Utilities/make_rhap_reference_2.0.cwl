requirements:
  DockerRequirement:
    dockerPull: bdgenomics/rhapsody:2.0
  InlineJavascriptRequirement: {}
class: CommandLineTool
label: Reference Files Generator for BD Rhapsody™ Sequencing Analysis Pipeline
cwlVersion: v1.2
doc: >- 
    The Reference Files Generator creates an archive containing Genome Index and Transcriptome annotation files needed for the BD Rhapsody™ Sequencing Analysis Pipeline. The app takes as input one or more FASTA and GTF files and produces a compressed archive in the form of a tar.gz file. The archive contains:\n  - STAR index\n  - Filtered GTF file


baseCommand: run_reference_generator.sh
inputs: 
    Genome_fasta:
        type: File[]
        label: Reference Genome
        doc: |-
            Reference genome file in FASTA format. The BD Rhapsody™ Sequencing Analysis Pipeline uses GRCh38 for Human and GRCm39 for Mouse.
        inputBinding:
            prefix: --reference-genome
            shellQuote: false
            position: 0
    Gtf:
        type: File[]
        label: Transcript Annotations
        doc: |-
            Transcript annotation files in GTF format. The BD Rhapsody™ Sequencing Analysis Pipeline uses Gencode v42 for Human and M31 for Mouse.
        inputBinding:
            prefix: --gtf
            shellQuote: false
            position: 1
    Extra_sequences:
        type: File[]?
        label: Extra Sequences
        doc: |-
            Additional sequences in FASTA format to use when building the STAR index. (E.g. phiX genome)
        inputBinding:
            prefix: --extra-sequences
            shellQuote: false
            position: 2
    Filtering_off:
        type: boolean?
        label: Turn off filtering
        doc: |-
            By default the input Transcript Annotation files are filtered based on the gene_type/gene_biotype attribute. Only features having the following attribute values are are kept: \n  - protein_coding \n  - lncRNA (lincRNA and antisense for Gencode < v31/M22/Ensembl97)\n  - IG_LV_gene\n  - IG_V_gene\n  - IG_V_pseudogene\n  - IG_D_gene\n  - IG_J_gene\n  - IG_J_pseudogene\n  - IG_C_gene\n  - IG_C_pseudogene\n  - TR_V_gene\n  - TR_V_pseudogene\n  - TR_D_gene\n  - TR_J_gene\n  - TR_J_pseudogene\n  - TR_C_gene\n\nIf you have already pre-filtered the input Annotation files and/or wish to turn-off the filtering, please set this option to True.
        inputBinding: 
            prefix: --filtering-off
            shellQuote: false
            position: 3
    Archive_prefix:
        type: string?
        label: Archive Prefix
        doc: |-
            A prefix for naming the compressed archive file containing the Reference genome index and annotation files. The default value is constructed based on the input Reference files.
        inputBinding:
            prefix: --archive-prefix
            shellQuote: false
            position: 4
    
    Maximum_threads:
        type: int?
        label: Maximum Number of Threads
        doc: |-
            The maximum number of threads to use in the pipeline. By default, all available cores are used.
        inputBinding:
            prefix: --maximum-threads
            shellQuote: false
            position: 5


#stdout: MakeRhapRef.log
#stderr: MakeRhapRefErr.log
outputs:
    log:
        type: File?
        outputBinding:
            glob: "*.log"
    Archive:
        type: File
        doc: |- 
            A Compressed archive containing the Reference Genome Index and annotation GTF files. This archive is meant to be used as an input in the BD Rhapsody™ Sequencing Analysis Pipeline.
        id: Reference_Archive
        label: Reference Files Archive
        outputBinding:
            glob: '*.tar.gz'

