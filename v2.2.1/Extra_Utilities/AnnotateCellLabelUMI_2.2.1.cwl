#!/usr/bin/env cwl-runner
cwlVersion: v1.2
doc: >
  This module is for the specific use of annotating reads
  with their cell label and UMI.
hints:
  DockerRequirement:
    dockerPull: bdgenomics/rhapsody:2.2.1
requirements:
  ShellCommandRequirement: {}
class: CommandLineTool
label: BD Annotate Cell Label and UMI
baseCommand: [mist_run_qualclalign.py, "--disable-alignment"]
inputs:
  Reads:
    type: File[]
    inputBinding:
      prefix: "--reads"
      itemSeparator: ','
  Maximum_Threads:
    type: int?
    inputBinding:
      prefix: "--threads"
  Run_Name:
    type: string?
    inputBinding:
      prefix: "--run-name"
  Assay:
    type: string?
    default: "WTA"
    inputBinding:
      prefix: "--assay"
outputs:
  AnnotatedFastq:
    outputBinding:
      glob: "*.fastq.gz"
    type: File[]
  QualCLAlignMetrics:
    outputBinding:
      glob: "*ReadQualityMetrics.json"
    type: File
